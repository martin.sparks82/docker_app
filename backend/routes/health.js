const express = require('express');
const HttpStatus = require('http-status-codes');

const router = express.Router();


router.get('/', (req, res, next) => {
  res.status(HttpStatus.OK).json({status: 'OK'})
});

module.exports = router;