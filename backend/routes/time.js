var express = require('express');
const HttpStatus = require('http-status-codes');

var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  const now = new Date(Date.now());

  res.status(HttpStatus.OK).json({
    hour: now.getHours(),
    minute: now.getMinutes(),
    second: now.getSeconds(),
    ms: now.getMilliseconds(),
  })
});

module.exports = router;
