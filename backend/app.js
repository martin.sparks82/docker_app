var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');

var indexRouter = require('./routes/index');
const timeRouter = require('./routes/time');
const healthCheckRouter = require('./routes/health');

var app = express();

app.use(logger('dev'));
app.use(cors({origin: '*'}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/time', timeRouter);
app.use('/healthz', healthCheckRouter);

module.exports = app;
