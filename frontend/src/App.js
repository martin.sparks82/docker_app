import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const [time, setTime] = useState()

  const getTime = () =>{
    fetch("http://localhost:3000/time")
    .then(res => res.json())
    .then(timeAtLoad =>  setTime(timeAtLoad))
  }

  useEffect(() => {
    getTime()
  }, [])

  const formatTime = () => {
    if (time){
      const hour = time.hour.toString().padStart(2, "0");
      const minute = time.minute.toString().padStart(2, "0");
      const second = time.second.toString().padStart(2, "0");
  
      return `${hour}:${minute}:${second}`
    }
  }

  return (
    <>
      <h1>Welcome</h1>
      <h2>The time is: {formatTime()}</h2>
    </>
  );
}

export default App;
