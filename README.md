Junior Platform Engineer
========================

Technical Assessment
--------------------

The purpose of this assessment is to provide an example of basic tasks that will form some parts of the role. The output of the following tasks will also form the basis of the technical interview.
In completing the assessment, feel free to use as many external resources as you wish (Google, StackOverflow, etc) however you should be able to explain the work you completed in the interview.
For Part II, you do not need to submit answers to these questions, a random subset of these questions will be asked in the interview, they are provided so you can research the various areas.
Other questions may be asked around these areas as well, but they will generally be limited to things that would come up in researching answers to these questions.

Part I: Docker
--------------

There are two folders in this repo that form a very simple frontend and backend application. Your first task is to dockerise these applications. Simply create 2 `Dockerfile`s, one for each application.

Part II: Questions
------------------

1. [Containerisation] What is the difference between a container and a virtual machine?
2. [Infrastructure] What is Infrastructure as Code (IaC)? 
3. [Kubernetes] What is the difference between a StatefulSet, a Deployment and a DaemonSet?
4. [Kubernetes] What is an Ingress used for? What is a Service used for?
5. [Google Cloud] What is the difference between a Region and a Zone?
6. [Networking] What is DNS? 
7. [Redis] What are the pros and cons of hosting a redis cluster vs a redis failover?
8. [Security] What is the principle of least privilege?
